using UnityEngine;
using System.Collections;

/// <summary>
/// 	Curiosity undercarriage implementation
/// </summary>
public class CuriosityUndercarriage : MonoBehaviour, IUnderCarriage{

	/// <summary>
	/// 	Base torque applied to wheels to move
	/// </summary>
	public float maxMoveTorque = 100;

	/// <summary>
	/// 	Based on baseMoveTorque, set depending on current height 
	/// </summary>
	private float currentMaxMoveTorque;
	
	/// <summary>
	/// 	Max speed (wheels velocity)
	/// </summary>
	public float maxSpeed = 1000;

	/// <summary>
	/// 	Based on maxSpeed, set depending on current height
	/// </summary>
	private float currentMaxSpeed;
	
	/// <summary>
	/// 	Torque applied to wheels to steer
	/// </summary>
	public float steerTorque = 1000;
	
	/// <summary>
	/// 	Max angle for steering
	/// </summary>
	public float maxSteerAngle = 20;

	/// Ease access to positionned elements
	private enum ElementPosition 
	{	
		LEFT_FRONT, 
		RIGHT_FRONT,
		LEFT_MIDDLE,
		RIGHT_MIDDLE,
		LEFT_REAR,
		RIGHT_REAR
	};
	
	/// Arms references and settings
	private HingeJoint[] armsJoints;
	private JointSpring[] armsJointsSpring;
	private JointLimits[] armsJointsLimits;
	
	/// Wheels supports references and settings
	private HingeJoint[] wheelSupportsJoints;
	
	/// Wheels references and settings
	private HingeJoint[] wheelsJoints;
	
	/// Tmp variables for modifying springs and motors
	private JointSpring moveAndSteerJointSpring;
	private JointMotor moveAndSteerJointMotor;

	/// <summary>
	/// 	Start this instance.
	/// </summary>
	void Start ()
	{
		//GameObject.Find ("body").GetComponent<Rigidbody> ().centerOfMass = new Vector3 (0, -1, 0);

		//Init arms
		this.armsJoints = new HingeJoint[6];
		this.armsJointsSpring = new JointSpring[6];
		this.armsJointsLimits = new JointLimits[6];
		this.armsJoints[(int)ElementPosition.LEFT_FRONT] = GameObject.Find ("armLF").GetComponent<HingeJoint>();
		this.armsJoints[(int)ElementPosition.RIGHT_FRONT] = GameObject.Find ("armRF").GetComponent<HingeJoint>();
		this.armsJoints[(int)ElementPosition.LEFT_MIDDLE] = GameObject.Find ("subarmLM").GetComponent<HingeJoint>();
		this.armsJoints[(int)ElementPosition.RIGHT_MIDDLE] = GameObject.Find ("subarmRM").GetComponent<HingeJoint>();
		this.armsJoints[(int)ElementPosition.LEFT_REAR] = GameObject.Find ("armLM").GetComponent<HingeJoint>();
		this.armsJoints[(int)ElementPosition.RIGHT_REAR] = GameObject.Find ("armRM").GetComponent<HingeJoint>();
		for(int i = 0; i < this.armsJoints.Length; i++){
			this.armsJointsSpring[i] = this.armsJoints[i].spring;
			this.armsJointsLimits[i] = this.armsJoints[i].limits;
		}
		
		//Init wheel supports
		this.wheelSupportsJoints = new HingeJoint[6];
		this.wheelSupportsJoints[(int)ElementPosition.LEFT_FRONT] = GameObject.Find ("wheelSupportLF").GetComponent<HingeJoint>();
		this.wheelSupportsJoints[(int)ElementPosition.RIGHT_FRONT] = GameObject.Find ("wheelSupportRF").GetComponent<HingeJoint>();
		this.wheelSupportsJoints[(int)ElementPosition.LEFT_REAR] = GameObject.Find ("wheelSupportLR").GetComponent<HingeJoint>();
		this.wheelSupportsJoints[(int)ElementPosition.RIGHT_REAR] = GameObject.Find ("wheelSupportRR").GetComponent<HingeJoint>();
		
		//Init wheels
		this.wheelsJoints = new HingeJoint[6];
		this.wheelsJoints[(int)ElementPosition.LEFT_FRONT] = GameObject.Find ("wheelLF").GetComponent<HingeJoint>();
		this.wheelsJoints[(int)ElementPosition.RIGHT_FRONT] = GameObject.Find ("wheelRF").GetComponent<HingeJoint>();
		this.wheelsJoints[(int)ElementPosition.LEFT_MIDDLE] = GameObject.Find ("wheelLM").GetComponent<HingeJoint>();
		this.wheelsJoints[(int)ElementPosition.RIGHT_MIDDLE] = GameObject.Find ("wheelRM").GetComponent<HingeJoint>();
		this.wheelsJoints[(int)ElementPosition.LEFT_REAR] = GameObject.Find ("wheelLR").GetComponent<HingeJoint>();
		this.wheelsJoints[(int)ElementPosition.RIGHT_REAR] = GameObject.Find ("wheelRR").GetComponent<HingeJoint>();

		//Init speed and torque
		this.currentMaxSpeed = this.maxSpeed / 2;
		this.currentMaxMoveTorque = this.maxMoveTorque * 2;

		//Init inner TMP variables
		this.moveAndSteerJointSpring = new JointSpring();
		this.moveAndSteerJointSpring.damper = 1;
		this.moveAndSteerJointMotor = new JointMotor();
		this.moveAndSteerJointMotor.force = this.currentMaxMoveTorque;
		this.moveAndSteerJointMotor.freeSpin = true;

	}

	/// <summary>
	/// 	Update this instance once per frame
	/// </summary>
	void Update ()
	{
		//NOP
	}

	/// <summary>
	/// 	Update the physics regularly in this instance
	/// </summary>
	void FixedUpdate ()
	{
		//NOP
	}

	#region IUnderCarriage implementation

	public void moveAndSteer (float moveMagnitude, float steerMagnitude)
	{
		//Steer
		float targetPosition = steerMagnitude * maxSteerAngle;
		this.moveAndSteerJointSpring.spring = steerTorque;
		this.moveAndSteerJointSpring.targetPosition = targetPosition;
		this.wheelSupportsJoints [(int)ElementPosition.LEFT_FRONT].spring = this.moveAndSteerJointSpring;
		this.wheelSupportsJoints [(int)ElementPosition.RIGHT_FRONT].spring = this.moveAndSteerJointSpring;
		this.moveAndSteerJointSpring.targetPosition = -targetPosition;
		this.wheelSupportsJoints [(int)ElementPosition.LEFT_REAR].spring = this.moveAndSteerJointSpring;
		this.wheelSupportsJoints [(int)ElementPosition.RIGHT_REAR].spring = this.moveAndSteerJointSpring;
		
		//Move
		if (steerMagnitude > 0) {
			this.moveAndSteerJointMotor.targetVelocity = moveMagnitude * this.currentMaxSpeed;
			this.wheelsJoints[(int)ElementPosition.LEFT_FRONT].motor = this.moveAndSteerJointMotor;
			this.wheelsJoints[(int)ElementPosition.LEFT_MIDDLE].motor = this.moveAndSteerJointMotor;
			this.wheelsJoints[(int)ElementPosition.LEFT_REAR].motor = this.moveAndSteerJointMotor;
			this.moveAndSteerJointMotor.targetVelocity = moveMagnitude * this.maxSpeed * Mathf.Sin(targetPosition *  Mathf.Deg2Rad);
			this.wheelsJoints[(int)ElementPosition.RIGHT_FRONT].motor = this.moveAndSteerJointMotor;
			this.wheelsJoints[(int)ElementPosition.RIGHT_MIDDLE].motor = this.moveAndSteerJointMotor;
			this.wheelsJoints[(int)ElementPosition.RIGHT_REAR].motor = this.moveAndSteerJointMotor;
		} else if (steerMagnitude < 0) {
			this.moveAndSteerJointMotor.targetVelocity = moveMagnitude * this.currentMaxSpeed;
			this.wheelsJoints[(int)ElementPosition.RIGHT_FRONT].motor = this.moveAndSteerJointMotor;
			this.wheelsJoints[(int)ElementPosition.RIGHT_MIDDLE].motor = this.moveAndSteerJointMotor;
			this.wheelsJoints[(int)ElementPosition.RIGHT_REAR].motor = this.moveAndSteerJointMotor;
			this.moveAndSteerJointMotor.targetVelocity = moveMagnitude * this.maxSpeed * Mathf.Sin(-targetPosition *  Mathf.Deg2Rad);
			this.wheelsJoints[(int)ElementPosition.LEFT_FRONT].motor = this.moveAndSteerJointMotor;
			this.wheelsJoints[(int)ElementPosition.LEFT_MIDDLE].motor = this.moveAndSteerJointMotor;
			this.wheelsJoints[(int)ElementPosition.LEFT_REAR].motor = this.moveAndSteerJointMotor;
		} else {
			this.moveAndSteerJointMotor.targetVelocity = moveMagnitude * this.currentMaxSpeed;
			this.wheelsJoints[(int)ElementPosition.RIGHT_FRONT].motor = this.moveAndSteerJointMotor;
			this.wheelsJoints[(int)ElementPosition.RIGHT_MIDDLE].motor = this.moveAndSteerJointMotor;
			this.wheelsJoints[(int)ElementPosition.RIGHT_REAR].motor = this.moveAndSteerJointMotor;
			this.wheelsJoints[(int)ElementPosition.LEFT_FRONT].motor = this.moveAndSteerJointMotor;
			this.wheelsJoints[(int)ElementPosition.LEFT_MIDDLE].motor = this.moveAndSteerJointMotor;
			this.wheelsJoints[(int)ElementPosition.LEFT_REAR].motor = this.moveAndSteerJointMotor;
		}	


	}

	public void setDamper (float damperMagnitude)
	{
		for (int i = 0; i < this.armsJoints.Length; i++) {
			JointSpring tmpJointSpring = this.armsJoints[i].spring;
			tmpJointSpring.damper =  this.armsJointsSpring[i].damper * damperMagnitude;
			tmpJointSpring.spring =  this.armsJointsSpring[i].spring * damperMagnitude;
			this.armsJoints[i].spring = tmpJointSpring;
		}
	}

	public void setHeight (float heightMagnitude)
	{
		for (int i = 0; i < this.armsJoints.Length; i++) {
			JointSpring tmpJointSpring = this.armsJoints [i].spring;
			tmpJointSpring.targetPosition =  (heightMagnitude * -(this.armsJointsLimits[i].min)) +  (this.armsJointsLimits[i].min);
			this.armsJoints[i].spring = tmpJointSpring;
		}

		//Adapt speed and torque to height
		this.currentMaxSpeed = Mathf.Min(this.maxSpeed, (this.maxSpeed / 2) / heightMagnitude);
		this.currentMaxMoveTorque = Mathf.Max(this.maxMoveTorque, (this.maxMoveTorque * 2) * heightMagnitude);
		this.moveAndSteerJointMotor.force = this.currentMaxMoveTorque;
	}

	#endregion
}

