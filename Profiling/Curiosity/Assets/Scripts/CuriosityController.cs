﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 	Curiosity controller (TESTING PURPOSE ONLY).
/// </summary>
public class CuriosityController : MonoBehaviour{

	/// <summary>
	/// 	Reference to the attached IUnderCarriage instance
	/// </summary>
	private IUnderCarriage underCarriage;

	private float currentDamperMagnitude = 1f;

	private float currentHeightMagnitude = 1f;

	/// <summary>
	/// 	Use this for initialization
	/// </summary>
	void Start () {
		this.underCarriage = this.GetComponent (typeof(IUnderCarriage)) as IUnderCarriage;

		if (this.underCarriage == null) {
			throw new System.MissingMemberException("Missing IUnderCarriage");
		}
	}

	/// <summary>
	/// 	Update is called once per frame
	/// </summary>
	void Update () {

	}

	/// <summary>
	/// 	FixedUpdate is called for physic updates 
	/// </summary>
	void FixedUpdate(){
		//Damper
		if (Input.GetKey (KeyCode.D)) {
			if(this.currentDamperMagnitude < 2f)this.currentDamperMagnitude += 0.05f;
			this.underCarriage.setDamper(this.currentDamperMagnitude);
		}
		else if (Input.GetKey (KeyCode.C)) {
			if(this.currentDamperMagnitude > 0.05f)  this.currentDamperMagnitude -= 0.05f;
			this.underCarriage.setDamper(this.currentDamperMagnitude);
		}

		//Height
		if (Input.GetKey (KeyCode.H)) {
			if(this.currentHeightMagnitude < 2f) this.currentHeightMagnitude += 0.05f;
			this.underCarriage.setHeight(currentHeightMagnitude);
		}
		else if (Input.GetKey (KeyCode.N)) {
			if(this.currentHeightMagnitude > 0.05f) this.currentHeightMagnitude -= 0.05f;
			this.underCarriage.setHeight(currentHeightMagnitude);
		}

		//Move and steer
		this.underCarriage.moveAndSteer (Input.GetAxis ("Vertical"), Input.GetAxis ("Horizontal"));
	}
}
