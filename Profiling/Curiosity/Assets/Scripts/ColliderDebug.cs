﻿using UnityEngine;
using System.Collections;

public class ColliderDebug : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionStay(Collision collisionInfo) {
		Debug.Log(this.name+" -> " +collisionInfo.relativeVelocity);
	}
}
