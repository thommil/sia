using System;

// Interface defining UnderCarriage API
public interface IUnderCarriage{
	
	/// <summary>
	/// Moves and steers vehicle based on move and steer magnitude
	/// </summary>
	/// <param name="moveMagnitude">Move magnitude [-1, 1].</param>
	/// <param name="steerMagnitude">Steer magnitude [-1, 1].</param>
	void moveAndSteer(float moveMagnitude, float steerMagnitude);

	/// <summary>
	/// Sets damper hardeness based on magnitude
	/// </summary>
	/// <param name="damperMagnitude">Damper magnitude [0,...,1,...,n].</param>
	void setDamper (float damperMagnitude);

	/// <summary>
	/// Sets undercarriage height base on magnitude
	/// </summary>
	/// <param name="heightMagnitude">Height magnitude [0,...,1,...,n].</param>
	void setHeight (float heightMagnitude);
}


